<?php

namespace App\Http\Controllers;
use DataTables;
use App\DataTables\UserDataTable;
use App\User;
use Illuminate\Http\Request;

class YAjraDataTableController extends Controller
{
    // public function showtable(UserDataTable $datatable)
    // {
        
    //     return $datatable->render('YajraDatatables');
    // }



    public function showtable(Request $request)
    {
        if($request->ajax())
        {
            
            $users = User::select('id', 'name', 'email', 'created_at', 'updated_at')->get();

            return DataTables::of($users)
            
                ->addColumn('created_at', function($row) {

                    return '<span>' . date_format(date_create($row->created_At), 'm/d/Y h:i a') . '</span>';
                    
                })
                

         
                ->make(true);
        }
        
        return view('YajraDatatables');
        
       
    }




    
}
