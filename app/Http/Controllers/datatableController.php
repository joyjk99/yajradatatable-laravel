<?php
namespace App\Http\Controllers;
use DataTables;
use App\DataTables\UserDataTable;
use App\User;
use Illuminate\Http\Request;

class datatableController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            
           if(!empty($request->filter_country))
           {

                    
            $users = User::select('id', 'name', 'email', 'country', 'created_at', 'updated_at')
            
            ->where('country',$request->filter_country)->get();

            return DataTables::of($users)
            
                ->addColumn('created_at', function($row) {

                    return '<span>' . date_format(date_create($row->created_At), 'm/d/Y h:i a') . '</span>';
                    
                })
                ->addColumn('id', function($row) {

                    return '<span data-orig-value="' .$row->id. '" class="calculation" >' .$row->id. '</span>';
                    
                })
                ->addColumn('updated_at', function($row) {

                    return '<span>' . date_format(date_create($row->updated_at), 'm-d-Y h:i a') . '</span>';
                    
                })
                ->addColumn('button', function($row) {

                   return '<a class="btn btn-danger" href="select/'.$row->id.'"> Hello  '.$row->id .'</a>';
                   //return "123";
                    
                })
                ->addColumn('dropdown', function($row) {

                    return '
                    
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            Dropdown button
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item" href="#" id="alertID" data-id="'.$row->name.'">Alert Name</a></li>
                            <li><a class="dropdown-item" href="#" id="alertEmail" data-id="'.$row->email.'">Alert Email</a></li>
                            <li><a class="dropdown-item" href="#" id="deleteSomething" data-id="'.$row->id.'">Delete</a></li>

                            
                        </ul>
                    </div>
                    
                    ';
                    
                     
                 })

                 ->addColumn('add', function($row) {
                    
                    $total=0;
                    $data = $row->id;
                    foreach($row as $ids)
                    {
                        $total+=$ids;
                    }

                    return '<a class="btn btn-primary" > Hello  '.$total.'</a>';
                   
                     
                 })

                //delete/'.$row->id.'

                ->rawColumns(['created_at','updated_at','button','dropdown','add','id'])

         
                ->make(true);

           }
           else
           {
                        
                    $users = User::select('id', 'name', 'email', 'country', 'created_at', 'updated_at')->get();

                    return DataTables::of($users)
                    
                        ->addColumn('created_at', function($row) {

                            return '<span>' . date_format(date_create($row->created_At), 'm/d/Y h:i a') . '</span>';
                            
                        })
                        ->addColumn('id', function($row) {

                            return '<span data-orig-value="' .$row->id. '" class="calculation" >' .$row->id. '</span>';
                            
                        })
                        ->addColumn('updated_at', function($row) {

                            return '<span>' . date_format(date_create($row->updated_at), 'm-d-Y h:i a') . '</span>';
                            
                        })
                        ->addColumn('button', function($row) {

                        return '<a class="btn btn-danger" href="select/'.$row->id.'"> Hello  '.$row->id .'</a>';
                        //return "123";
                            
                        })
                        ->addColumn('dropdown', function($row) {

                            return '
                            
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                    Dropdown button
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li><a class="dropdown-item" href="#" id="alertID" data-id="'.$row->name.'">Alert Name</a></li>
                                    <li><a class="dropdown-item" href="#" id="alertEmail" data-id="'.$row->email.'">Alert Email</a></li>
                                    <li><a class="dropdown-item" href="#" id="deleteSomething" data-id="'.$row->id.'">Delete</a></li>

                                    
                                </ul>
                            </div>
                            
                            ';
                            
                            
                        })

                        ->addColumn('add', function($row) {
                            
                            $total=0;
                            $data = $row->id;
                            foreach($row as $ids)
                            {
                                $total+=$ids;
                            }

                            return '<a class="btn btn-primary" > Hello  '.$total.'</a>';
                        
                            
                        })

                        //delete/'.$row->id.'

                        ->rawColumns(['created_at','updated_at','button','dropdown','add','id'])

                
                        ->make(true);
           }
        }
        
        $users = User::select('country')->distinct('country')->get();
        
        //dd($countries->first());
        return view('datatableDemo', [ "users" => $users ]);

       // return view('datatableDemo');
        
       
    }

    public function deleteUser(Request $req)
    {
        
        $user = User::find($req->id);

        $user->delete();

        return redirect('/datatable');
    }
}
