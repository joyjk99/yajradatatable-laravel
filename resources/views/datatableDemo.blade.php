<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">



    <!---Updated Cdns -->

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.11.3/datatables.min.css" />

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-3.6.0/jszip-2.5.0/dt-1.11.3/b-2.0.1/b-colvis-2.0.1/b-html5-2.0.1/b-print-2.0.1/cr-1.5.4/date-1.1.1/fc-4.0.0/fh-3.2.0/kt-2.6.4/r-2.2.9/rg-1.1.3/rr-1.2.8/sc-2.0.5/sb-1.2.2/sp-1.4.0/sl-1.3.3/datatables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    <title>My new DataTable</title>

</head>

<body>
    <div class="container">
        <div class="row">
            <div style="margin-left:40%">

                <select name="filter_country" id="filter_country">
                    <option value="">Select Country</option>
                    @foreach($users as $user)

                    <option value="{{$user->country}}">{{$user->country}}</option>
                    @endforeach

                </select>
                <!-- <button type="button" name="filter" id="filter" class="btn btn-info">Filter</button> -->
            </div>
        </div>
    </div>

    <div class="container">

        <div class="row">

            <div class="col-md-12" style="margin:10% 5% 10% 5%">

                <table id="user_table" class="table table-bordered table-hover table-striped">

                    <thead>

                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Country</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Button</th>
                            <th>Dropdown</th>
                            <th>Add</th>

                        </tr>

                    </thead>


                    <tfoot>

                        <tr>
                            <th></th>
                            <th id="datavalue"></th>

                        </tr>

                    </tfoot>

                </table>

            </div>

        </div>

    </div>


</body>

</html>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.11.3/datatables.min.js"></script>



<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-3.6.0/jszip-2.5.0/dt-1.11.3/b-2.0.1/b-colvis-2.0.1/b-html5-2.0.1/b-print-2.0.1/cr-1.5.4/date-1.1.1/fc-4.0.0/fh-3.2.0/kt-2.6.4/r-2.2.9/rg-1.1.3/rr-1.2.8/sc-2.0.5/sb-1.2.2/sp-1.4.0/sl-1.3.3/datatables.min.js"></script>



<script>
    $(document).ready(function() {

        fill_datatable();

        function fill_datatable(filter_country = '') {


            $('#user_table').DataTable({

                processing: true,

                serverside: true,

                dom: 'Bfrtip',
                buttons: [{
                        extend: 'collection',
                        text: 'Export',
                        buttons: [
                            'copy',
                            'excel',
                            'csv',
                            'pdf',
                            'print',
                        ],
                    },

                ],

                //region

                // initComplete: function() {
                //         this.api().columns().every(function(rowIdx) {
                //             var column = this;
                //             var placeHolder = '';
                //             placeHolder = 'Select ' + $(this.header()).html()
                //             if (column.index() < 3) {
                //                 var select = $('<select><option value="">' + placeHolder + '</option></select>')
                //                     .appendTo($(column.header()).empty())
                //                     .on('change', function() {
                //                         var val = $.fn.dataTable.util.escapeRegex(
                //                             $(this).val()
                //                         );

                //                         column
                //                             .search(val ? '^' + val + '$' : '', true, false)
                //                             .draw();
                //                     });

                //                 column.data().unique().sort().each(function(d, j) {
                //                     select.append('<option value="' + d + '">' + d + '</option>')
                //                 });
                //             }
                //         });
                //     },

                //endregion

                searching: true,
                "pageLength": 5,

                ajax: {
                    url: "/datatable",
                    data: {
                        filter_country: filter_country
                    }


                    // "data": function(d){

                    // },
                    // "dataFilter": function(data){
                    //     var json = jQuery.parseJSON(data);
                    //     json.draw = json.draw;
                    //     json.recordsFiltered = json.total;
                    //     json.recordsTotal = json.total;
                    //     json.data = json.data;

                    //     return JSON.stringify(json);

                    // }


                },




                columns: [

                    {
                        data: 'id',
                        name: 'id'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'country',
                        name: 'country'
                    },
                    {
                        data: 'created_at',
                        name: 'created_at'
                    },
                    {
                        data: 'updated_at',
                        name: 'updated_at'
                    },
                    {
                        data: 'button',
                        name: 'button'
                    },
                    {
                        data: 'dropdown',
                        name: 'dropdown'
                    },
                    {
                        data: 'add',
                        name: 'add'
                    },

                ],

                // "drawCallback":function( settings  ) {
                //     // let total = sum_table_col($("#user_table"),"calculation");

                //     // $("#datavalue").text() = "$ "+total;
                //     alert("HHI")
                // },


                "footerCallback": function(row, data, start, end, display) {
                    var api = this.api(),
                        data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function(i) {
                        return i;
                    };

                    // Total over all pages
                    total = api
                        .column(0)
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Total over this page
                    pageTotal = api
                        .column(0, {
                            page: 'current'
                        })
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Update footer
                    $(api.column(3).footer()).html(
                        '$' + pageTotal + ' ( $' + total + ' total)'
                    );
                }
            });
        }

        $('#filter_country').change(function() { //filter

            var filter_country = $('#filter_country').val();

            if (filter_country != '') {
                $('#user_table').DataTable().destroy();
                fill_datatable(filter_country);
            } else {
                $('#user_table').DataTable().destroy();
                fill_datatable();
            }
        });

        $(document).on("click", "#alertID", function() {
            var alert_name = $(this).data("id");
            alert(alert_name);
        })

        $(document).on("click", "#alertEmail", function() {
            var email_id = $(this).data("id");
            alert(email_id);
        })

        $(document).on("click", "#deleteSomething", function() {
            var something_id = $(this).data("id");
            // alert(something_id);

            var txt;
            var r = confirm("Do You Went to delete");
            if (r == true) {
                window.location.href = "/delete/" + something_id;
            } else {
                // alert("Bye")
            }


        })


    });
</script>