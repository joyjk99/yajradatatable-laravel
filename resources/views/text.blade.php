<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>



    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                initComplete: function() {
                    this.api().columns().every(function(rowIdx) {
                        var column = this;
                        var placeHolder = '';
                        placeHolder = 'Select ' + $(this.header()).html()
                        if (column.index() < 3) {
                            var select = $('<select><option value="">' + placeHolder + '</option></select>')
                                .appendTo($(column.header()).empty())
                                .on('change', function() {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                                });

                            column.data().unique().sort().each(function(d, j) {
                                select.append('<option value="' + d + '">' + d + '</option>')
                            });
                        }
                    });
                }
            });
        });
    </script>
</body>

</html>